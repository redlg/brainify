var gulp         = require('gulp'),
    sass         = require('gulp-sass'), //препроцессор для sass/scss
    watch        = require('gulp-watch'), //наблюдение за изменениями в файлах
    uglify       = require('gulp-uglify'), //минификация js
    concat       = require('gulp-concat'), //конкатенация файлов
    source       = require('gulp-sourcemaps'), //маппинг
    cssnano      = require('gulp-cssnano'), //минификация css
    plumber      = require('gulp-plumber'), //отлавливает ошибки чтобы не прерывался watch
    imagemin     = require('gulp-imagemin'), //оптимизация изображений
    pngquant     = require('imagemin-pngquant'), //алгоритм для png
    spritesmith  = require('gulp.spritesmith'), //сборка спрайтов
    autoprefixer = require('gulp-autoprefixer'); //автопрефиксер css

var path = {
    build: {
        js:     './app/js/',
        css:    './app/css/',
        images: './app/i/min/',
        sprite: './app/i/origin/'
    },
    src: {
        js:             './app/js/scripts/*.js',
        styles:         './app/css/scss/global.scss',
        sprite:         './app/i/sprite/*.*',
        spriteTemplate: './app/css/scss/utils/sprite-template',
        spriteCss:      './app/css/scss/utils/',
        images:         './app/i/origin/*.*',
        libStyles:      './app/css/libs/*.css',
        libJS:          './app/js/libs/'
    },
    watch: {
        js:        './app/js/scripts/*.js',
        styles:    './app/css/scss/**/*.scss',
        images:    './app/i/origin/*.*',
        sprite:    './app/i/sprite/*.*',
        libStyles: './app/css/libs/*.css',
        libJS:     './app/js/libs/'
    }
};

//сборка scss
gulp.task('scss', function() {
    gulp.src([path.src.styles])
        .pipe(plumber())
        .pipe(source.init())
        .pipe(concat('global.min.css'))
        .pipe(sass())
        .pipe(autoprefixer())
        .pipe(cssnano())
        .pipe(source.write())
        .pipe(plumber.stop())
        .pipe(gulp.dest(path.build.css));
});

//сборка скриптов
gulp.task('scripts', function() {
    gulp.src([path.src.js])
        .pipe(plumber())
        .pipe(source.init())
        .pipe(concat('script.min.js'))
        .pipe(uglify())
        .pipe(source.write())
        .pipe(plumber.stop())
        .pipe(gulp.dest(path.build.js));
});

//сборка стилей библиотек
gulp.task('libStyles', function() {
    gulp.src([path.src.libStyles])
        .pipe(source.init())
        .pipe(concat('libs.min.css'))
        .pipe(sass())
        .pipe(autoprefixer())
        .pipe(cssnano())
        .pipe(source.write())
        .pipe(gulp.dest(path.build.css));
});

//сборка скриптов библиотек
gulp.task('libScripts', function() {
    gulp.src([
        path.src.libJS + 'jquery-3.1.1.min.js',
        path.src.libJS + '*.js'
        ])
        .pipe(concat('libs.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(path.build.js));
});

//оптимизация изображений
gulp.task('imagemin', function () {
    gulp.src(path.src.images)
        .pipe(plumber())
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(plumber.stop())
        .pipe(gulp.dest(path.build.images))
});

//сборка спрайта
gulp.task('sprite', function() {
    var spriteData =
        gulp.src(path.src.sprite)
            .pipe(spritesmith({
                imgName: 'sprite.png',
                cssName: '_sprite.scss',
                cssFormat: 'scss',
                algorithm: 'binary-tree',
                padding: 20,
                cssTemplate: path.src.spriteTemplate, //используемый шаблон для генерации css
                //cssVarMap: function(sprite) {
                //    sprite.name = 's-' + sprite.name
                //}
            }));
    spriteData.img.pipe(gulp.dest(path.build.sprite));
    spriteData.css.pipe(gulp.dest(path.src.spriteCss));
});

//final build
gulp.task('build', [
    'scss',
    'scripts',
    'sprite',
    'libStyles',
    'libScripts',
    'imagemin'
]);

//watcher
gulp.task('watch', function(){
    watch([path.watch.styles], function(event, cb) {
        gulp.start('scss');
    });
    watch([path.watch.js], function(event, cb) {
        gulp.start('scripts');
    });
    watch([path.watch.sprite], function(event, cb) {
        gulp.start('sprite');
    });
    watch([path.watch.libStyles], function(event, cb) {
        gulp.start('libStyles');
    });
    watch([path.watch.libJS], function(event, cb) {
        gulp.start('libScripts');
    });
});

gulp.task('default', ['build']);