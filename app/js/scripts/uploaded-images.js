$(document).ready(function() {
    // В dataTransfer помещаются изображения которые перетащили в область div
    jQuery.event.props.push('dataTransfer');

    // Кнопка выбора файлов
    var uploadButton = $('.uploaded-form__input');

    // Массив для всех изображений
    var dataArray = [];

    // При падении файла в зону загрузки
    $('.uploaded-form').on('drop', function(e) {
        // Передаем в files все полученные изображения
        var files = e.dataTransfer.files;
        loadInView(files);
    });

    // При нажатии на кнопку выбора файлов
    uploadButton.on('change', function() {
        // Заполняем массив выбранными изображениями
        var files = $(this)[0].files;
        // Передаем массив с файлами в функцию загрузки на предпросмотр
        loadInView(files);
        // Очищаем инпут файл путем сброса формы
        $('.uploaded-form').each(function(){
            this.reset();
        });
    });

    // Функция загрузки изображений на предросмотр
    function loadInView(files) {
        // Для каждого файла
        $.each(files, function(index, file) {

            //Оповещение при попытке загрузить не изображение
            if (!file.type.match('image.*')) {
                alert(file.name + ' не является изображением и не будет загружен');
                return;
            }

            // Создаем новый экземпляра FileReader
            var fileReader = new FileReader();
            // Инициируем функцию FileReader
            fileReader.onload = (function(file) {

                return function(e) {
                    // Помещаем URI изображения в массив
                    dataArray.push({name : file.name, value : this.result});
                    addImage((dataArray.length - 1));
                };

            })(files[index]);
            // Производим чтение картинки по URI
            fileReader.readAsDataURL(file);
        });
        return false;
    }

    //Добавления эскизов на страницу
    function addImage(ind) {
        // Если индекс отрицательный значит выводим весь массив изображений
        var start, end;
        if (ind < 0 ) {
            start = 0;
            end = dataArray.length;
        } else {
            // иначе только определенное изображение
            start = ind;
            end = ind + 1;
        }
        // размещаем загруженные изображения
        for (var i = start; i < end; i++) {
            $('.uploaded-images').prepend('<div id="img-' + i + '" class="upload-image" style="background:url(' + dataArray[i].value + ') 50% 50% / cover;">' +
                                            '<a href="#" id="drop-'+i+'" class="upload-image__delete">Удалить</a>' +
                                        '</div>');
        }
        return false;
    }

    // Удаление только выбранного изображения
    $(".uploaded-images").on("click","a[id^='drop']", function(e) {
        e.preventDefault();
        // получаем название id
        var elemId = $(this).attr('id');
        // делим строку id на 2 части
        var temp = elemId.split('-');
        // получаем индекс изображения в массиве
        dataArray.splice(temp[1], 1);
        // Удаляем старые эскизы
        $('.uploaded-images > .upload-image').remove();
        // Обновляем эскизы в соответствии с обновленным массивом
        addImage(-1);
    });

    // Простые стили для области перетаскивания
    $('.uploaded-form').on('dragenter', function() {
        var $label = $(this).find('.uploaded-form__label');
        $label.addClass('uploaded-form__label_dragged');
        return false;
    });

    $('.uploaded-form').on('drop', function() {
        var $label = $(this).find('.uploaded-form__label');
        $label.removeClass('uploaded-form__label_dragged');
        return false;
    });

    $('.uploaded-form').on('dragleave', function() {
        var $label = $(this).find('.uploaded-form__label');
        $label.removeClass('uploaded-form__label_dragged');
        return false;
    });
});